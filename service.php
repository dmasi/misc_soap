<?php

# include the nusoap file
require_once('nusoap.php');

# create instance of nusoap server
$server = new nusoap_server;
 
# configure the wsdl
$server->configureWSDL('server', 'urn:server');
 
# add the schema namespace
$server->wsdl->schemaTargetNamespace = 'urn:server';

# register our function
$server->register('pollServer',
				# our input
				array('value' => 'xsd:string'),
				# our output
				array('return' => 'xsd:string'),
						'urn:server',
						'urn:server#pollServer');
# our test function
function pollServer($value){
	 
	        if($value['value'] == 'Good'){

				return "The value of the server poll resulted in good information";

			} else {

				return "The value of the server poll showed poor information";
			}
}
 
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
 
$server->service($HTTP_RAW_POST_DATA);
