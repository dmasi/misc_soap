<?php

# include the nusoap file
require_once('nusoap.php');

# the url of this supposed server we will call
$url = 'http://localhost/soap/service.php?wsdl';

# create instance of nusoap client
$client = new nusoap_client($url);

# check for errors in setting up the client
$err = $client->getError();
# if there were errors lets notify the user 
if ($err) {
	    echo '<p><b>Error: ' . $err . '</b></p>';
}

# server expects an array with a key of value containing the value
$args = array('value' => 'Good');

# call the server with nusoap call method
# first param is the exposed function
# second param is the array of params we want to pass
$return = $client->call('pollServer', array($args));

# drink beer
echo "<p>Value returned from the server is: " . $return . "</p>";
